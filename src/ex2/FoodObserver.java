package ex2;

public interface FoodObserver {

    void notifyFoodTaken(FoodObservedAnimal animal);
}
