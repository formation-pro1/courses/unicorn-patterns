package ex2;

import java.util.ArrayList;
import java.util.List;

public abstract class FoodObservedAnimal implements Animal {
    private List<FoodObserver> foodObservers = new ArrayList<>();

    public void addFoodObserver(FoodObserver foodObserver) {
        this.foodObservers.add(foodObserver);
    }

    public void removeFoodObserver(FoodObserver foodObserver) {
        this.foodObservers.remove(foodObserver);
    }

    public void notifyObservers() {
        foodObservers.forEach(foodObserver -> foodObserver.notifyFoodTaken(this));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
