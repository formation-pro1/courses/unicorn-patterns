package ex2;

public class Sheep extends FoodObservedAnimal {

    public void acceptFarmer(Farmer farmer) {
        farmer.visitSheep(this);
    }

    public void eat(int i) {
        System.out.println("Sheep: happy to eat " + i + " units");
        this.notifyObservers();
    }
}
