package ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DayInFarm {

    public static void main(String[] args) {

        List<FoodObservedAnimal> animals = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            animals.add(new Unicorn());
        }
        for(int i = 0; i < 3; i++) {
            animals.add(new Sheep());
        }
        for(int i = 0; i < 10; i++) {
            animals.add(new Goat());
        }

        // Shuffle animals
        Collections.shuffle(animals);

        Farmer farmer = new Farmer();

        Farm farm = new Farm.FarmBuilder()
                .withAnimals(animals)
                .withFarmer(farmer)
                .build();
        farm.dayInFarm();
    }
}
