package ex2;

import java.util.List;

public class Farm {
    private List<FoodObservedAnimal> animals;
    private Farmer farmer;

    public Farm(List<FoodObservedAnimal> animals, Farmer farmer) {
        this.animals = animals;
        this.farmer = farmer;
        this.animals.forEach(animal -> animal.addFoodObserver(farmer));
    }

    public void dayInFarm() {
        animals.forEach(animal -> animal.acceptFarmer(farmer));
    }

    public static class FarmBuilder {
        private List<FoodObservedAnimal> animals;
        private Farmer farmer;

        public FarmBuilder withAnimals(List<FoodObservedAnimal> animals) {
            this.animals = animals;
            return this;
        }

        public FarmBuilder withFarmer(Farmer farmer) {
            this.farmer = farmer;
            return this;
        }

        public Farm build() {
            return new Farm(animals, farmer);
        }
    }
}
