package ex2;

public class Unicorn extends FoodObservedAnimal {

    public void acceptFarmer(Farmer farmer) {
        farmer.visitUnicorn(this);
    }

    public void eat(int i) {
        System.out.println("Unicorn: happy to eat " + i + " units");
        this.notifyObservers();
    }
}
