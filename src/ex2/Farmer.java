package ex2;

public class Farmer implements FoodObserver {

    public void visitUnicorn(Unicorn unicorn) {
        unicorn.eat(5);
    }

    public void visitSheep(Sheep sheep) {
        sheep.eat(2);
    }

    public void visitGoat(Goat goat) {
        goat.eat(3);
    }

    @Override
    public void notifyFoodTaken(FoodObservedAnimal animal) {
        System.out.println("Farmer: Yeah, " + animal + " has eaten");
    }

}
