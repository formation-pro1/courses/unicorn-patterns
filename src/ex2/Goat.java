package ex2;

public class Goat extends FoodObservedAnimal {

    public void acceptFarmer(Farmer farmer) {
        farmer.visitGoat(this);
    }

    public void eat(int i) {
        System.out.println("Goat: happy to eat " + i + " units");
        this.notifyObservers();
    }
}
