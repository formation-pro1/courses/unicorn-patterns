package ex1;

public class Environment {
    int clouds;
    int rocks;

    public Environment(int clouds, int rocks) {
        this.clouds = clouds;
        this.rocks = rocks;
    }

    @Override
    public String toString() {
        return "ex1.Environment{" +
                "clouds=" + clouds +
                ", rocks=" + rocks +
                '}';
    }
}
