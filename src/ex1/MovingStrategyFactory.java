package ex1;

public class MovingStrategyFactory {

    private static MovingStrategyFactory instance;

    private MovingStrategyFactory() {}

    public static MovingStrategyFactory getInstance() {
        if(instance == null) {
            instance = new MovingStrategyFactory();
        }
        return instance;
    }

    public MovingStrategy getStrategy(Environment environment) {
        if(environment.clouds < 1) {
            return new FlyStrategy();
        } else if (environment.rocks < 10) {
            return new GallopStrategy();
        } else {
            return new WalkStrategy();
        }
    }
}
