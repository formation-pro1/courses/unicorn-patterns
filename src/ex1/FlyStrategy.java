package ex1;

public class FlyStrategy implements MovingStrategy {

    @Override
    public int move() {
        return 50;
    }
}
