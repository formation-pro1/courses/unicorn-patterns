package ex1;

public class GallopStrategy implements MovingStrategy {

    @Override
    public int move() {
        return 20;
    }
}
