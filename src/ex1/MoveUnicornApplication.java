package ex1;

public class MoveUnicornApplication {

    public static void main(String[] args) {

        int distance;
        Unicorn unicorn = new Unicorn();

        MovingStrategyFactory movingStrategyFactory = MovingStrategyFactory.getInstance();

        Environment environment1 = new Environment(5, 6);
        unicorn.setMovingStrategy(movingStrategyFactory.getStrategy(environment1));
        distance = unicorn.move();
        System.out.println(String.format("La licorne s'est déplacée de %d mètres dans l'environnement %s", distance, environment1));

        Environment environment2 = new Environment(0, 14);
        unicorn.setMovingStrategy(movingStrategyFactory.getStrategy(environment2));
        distance = unicorn.move();
        System.out.println(String.format("La licorne s'est déplacée de %d mètres dans l'environnement %s", distance, environment2));

        Environment environment3 = new Environment(10, 20);
        unicorn.setMovingStrategy(movingStrategyFactory.getStrategy(environment3));
        distance = unicorn.move();
        System.out.println(String.format("La licorne s'est déplacée de %d mètres dans l'environnement %s", distance, environment3));

        Environment environment4 = new Environment(0, 20);
        unicorn.setMovingStrategy(movingStrategyFactory.getStrategy(environment4));
        distance = unicorn.move();
        System.out.println(String.format("La licorne s'est déplacée de %d mètres dans l'environnement %s", distance, environment4));

        Environment environment5 = new Environment(1, 20);
        unicorn.setMovingStrategy(movingStrategyFactory.getStrategy(environment5));
        distance = unicorn.move();
        System.out.println(String.format("La licorne s'est déplacée de %d mètres dans l'environnement %s", distance, environment5));
    }
}
