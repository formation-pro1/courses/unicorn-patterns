package ex1;

public class Unicorn{

    private MovingStrategy movingStrategy;

    public void setMovingStrategy(MovingStrategy movingStrategy) {
        this.movingStrategy = movingStrategy;
    }

    public int move() {
        return this.movingStrategy.move();
    }
}
