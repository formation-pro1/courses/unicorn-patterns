package ex1;

public interface MovingStrategy {
    int move();
}
